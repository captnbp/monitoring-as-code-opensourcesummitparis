provider "openstack" {
  auth_url = "https://auth.cloud.ovh.net/v3" # URL d'authentification
  domain_name = "default" # Nom de domaine - Toujours à "default" pour OVH
  alias = "ovh" # Un alias
}

provider "ovh" {
  endpoint = "ovh-eu" # Point d'entrée du fournisseur
  alias = "ovh" # Un alias
}
