#cloud-config
# vim: syntax=yaml

package_upgrade: false

runcmd:
  - sysctl net.bridge.bridge-nf-call-iptables=1
  - docker swarm init
  - docker network create -d overlay --attachable app-db
  - docker network create -d overlay --attachable admin-db
  - docker network create -d overlay --attachable admin-web
  - docker network create -d overlay --attachable app-web
  - docker network create -d overlay --attachable backend
  - docker node update mac --label-add db=true
  - docker node update mac --label-add admin-db=true
  - docker node update mac --label-add app-db=true
  - docker node update mac --label-add traefik=true


output: { all : '| tee -a /var/log/cloud-init-output.log' }

final_message: "The system is finally up, after $UPTIME seconds"

