#!/bin/sh
mkdir -p $HOME/.ssh
echo "${SSH_PRIVATE_KEY}" > $HOME/.ssh/id_rsa
chmod 600 $HOME/.ssh/id_rsa
ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no ubuntu@demoapp.doca-consulting.fr sudo docker service scale --detach=true demoapp_demoapp=0
ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no ubuntu@demoapp.doca-consulting.fr sudo docker service scale --detach=true demoapp_demoapp=1
