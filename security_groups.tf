resource "openstack_networking_secgroup_v2" "mac_prd" {
  name        = "mac_prd"
  description = "PRD and Admin"
  delete_default_rules = "true"
}

resource "openstack_networking_secgroup_rule_v2" "mac_prd_rule_ssh_4" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 22
  port_range_max    = 22
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = "${openstack_networking_secgroup_v2.mac_prd.id}"
}

resource "openstack_networking_secgroup_rule_v2" "mac_prd_rule_ssh_6" {
  direction         = "ingress"
  ethertype         = "IPv6"
  protocol          = "tcp"
  port_range_min    = 22
  port_range_max    = 22
  remote_ip_prefix  = "::/0"
  security_group_id = "${openstack_networking_secgroup_v2.mac_prd.id}"
}

resource "openstack_networking_secgroup_rule_v2" "mac_prd_rule_http_4" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 80
  port_range_max    = 80
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = "${openstack_networking_secgroup_v2.mac_prd.id}"
}

resource "openstack_networking_secgroup_rule_v2" "mac_prd_rule_traefik_4" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 8080
  port_range_max    = 8080
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = "${openstack_networking_secgroup_v2.mac_prd.id}"
}

resource "openstack_networking_secgroup_rule_v2" "mac_prd_rule_http_6" {
  direction         = "ingress"
  ethertype         = "IPv6"
  protocol          = "tcp"
  port_range_min    = 80
  port_range_max    = 80
  remote_ip_prefix  = "::/0"
  security_group_id = "${openstack_networking_secgroup_v2.mac_prd.id}"
}

resource "openstack_networking_secgroup_rule_v2" "mac_prd_rule_https_4" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 443
  port_range_max    = 443
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = "${openstack_networking_secgroup_v2.mac_prd.id}"
}

resource "openstack_networking_secgroup_rule_v2" "mac_prd_rule_https_6" {
  direction         = "ingress"
  ethertype         = "IPv6"
  protocol          = "tcp"
  port_range_min    = 443
  port_range_max    = 443
  remote_ip_prefix  = "::/0"
  security_group_id = "${openstack_networking_secgroup_v2.mac_prd.id}"
}

resource "openstack_networking_secgroup_rule_v2" "mac_prd_rule_vpn_4" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "udp"
  port_range_min    = 1194
  port_range_max    = 1194
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = "${openstack_networking_secgroup_v2.mac_prd.id}"
}

resource "openstack_networking_secgroup_rule_v2" "mac_prd_rule_vpn_6" {
  direction         = "ingress"
  ethertype         = "IPv6"
  protocol          = "udp"
  port_range_min    = 1194
  port_range_max    = 1194
  remote_ip_prefix  = "::/0"
  security_group_id = "${openstack_networking_secgroup_v2.mac_prd.id}"
}

resource "openstack_networking_secgroup_rule_v2" "mac_prd_rule_ssh_4_egress" {
  direction         = "egress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 22
  port_range_max    = 22
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = "${openstack_networking_secgroup_v2.mac_prd.id}"
}

resource "openstack_networking_secgroup_rule_v2" "mac_prd_rule_ssh_6_egress" {
  direction         = "egress"
  ethertype         = "IPv6"
  protocol          = "tcp"
  port_range_min    = 22
  port_range_max    = 22
  remote_ip_prefix  = "::/0"
  security_group_id = "${openstack_networking_secgroup_v2.mac_prd.id}"
}

resource "openstack_networking_secgroup_rule_v2" "mac_prd_rule_http_4_egress" {
  direction         = "egress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 80
  port_range_max    = 80
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = "${openstack_networking_secgroup_v2.mac_prd.id}"
}

resource "openstack_networking_secgroup_rule_v2" "mac_prd_rule_http_6_egress" {
  direction         = "egress"
  ethertype         = "IPv6"
  protocol          = "tcp"
  port_range_min    = 80
  port_range_max    = 80
  remote_ip_prefix  = "::/0"
  security_group_id = "${openstack_networking_secgroup_v2.mac_prd.id}"
}

resource "openstack_networking_secgroup_rule_v2" "mac_prd_rule_https_4_egress" {
  direction         = "egress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 443
  port_range_max    = 443
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = "${openstack_networking_secgroup_v2.mac_prd.id}"
}

resource "openstack_networking_secgroup_rule_v2" "mac_prd_rule_https_6_egress" {
  direction         = "egress"
  ethertype         = "IPv6"
  protocol          = "tcp"
  port_range_min    = 443
  port_range_max    = 443
  remote_ip_prefix  = "::/0"
  security_group_id = "${openstack_networking_secgroup_v2.mac_prd.id}"
}

resource "openstack_networking_secgroup_rule_v2" "mac_prd_rule_smtp_4_egress" {
  direction         = "egress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 587
  port_range_max    = 587
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = "${openstack_networking_secgroup_v2.mac_prd.id}"
}

resource "openstack_networking_secgroup_rule_v2" "mac_prd_rule_smtp_6_egress" {
  direction         = "egress"
  ethertype         = "IPv6"
  protocol          = "tcp"
  port_range_min    = 587
  port_range_max    = 587
  remote_ip_prefix  = "::/0"
  security_group_id = "${openstack_networking_secgroup_v2.mac_prd.id}"
}

resource "openstack_networking_secgroup_rule_v2" "mac_prd_rule_dns_4_egress" {
  direction         = "egress"
  ethertype         = "IPv4"
  protocol          = "udp"
  port_range_min    = 53
  port_range_max    = 53
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = "${openstack_networking_secgroup_v2.mac_prd.id}"
}

resource "openstack_networking_secgroup_rule_v2" "mac_prd_rule_dns_6_egress" {
  direction         = "egress"
  ethertype         = "IPv6"
  protocol          = "udp"
  port_range_min    = 53
  port_range_max    = 53
  remote_ip_prefix  = "::/0"
  security_group_id = "${openstack_networking_secgroup_v2.mac_prd.id}"
}

