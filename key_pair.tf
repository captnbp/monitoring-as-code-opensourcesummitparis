# Import de la clé SSH au sein d'OpenStack
resource "openstack_compute_keypair_v2" "mac_keypair" {
  provider = "openstack.ovh" # Nom du fournisseur déclaré dans provider.tf
  name = "mac_keypair" # Nom de la clé SSH à utiliser pour la création
  public_key = "${file("~/.ssh/id_rsa.pub")}" # Chemin vers votre clé SSH précédemment générée
}

