#!/bin/sh
# Make sure you replace the API and/or APP key below
# with the ones for your account

currenttime=$(date +%s)
currenttime2=$(date --date='10 minutes ago' +%s)
curl -G 'https://app.datadoghq.com/api/v1/events' \
    -d "start=${currenttime2}" \
    -d "end=${currenttime}" \
    -d "tags=instance:demoapp" \
    -d "api_key=${DATADOG_API_KEY}" \
    -d "application_key=${DATADOG_APP_KEY}" | grep "DemoApp is unreachable (HTTP Check) is critical on 1 over 1 host,instance,url"

if [ $? -eq 0 ]; then
	echo "Alert triggered. Test OK."
else
	echo "Alert NOT triggered. Test failed."
	exit 1
fi

