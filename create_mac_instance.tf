# Cherche l'image Archlinux la plus récente
data "openstack_images_image_v2" "ubuntu-mac" {
  name = "ubuntu-mac" # Nom de l'image
  most_recent = true # Limite la recherche à la plus récente
  provider = "openstack.ovh" # Nom du fournisseur
}


# Création d'une instance avec 2 interfaces réseau
resource "openstack_compute_instance_v2" "mac" {
  provider = "openstack.ovh" # Nom du fournisseur
  name = "mac" # Nom de l'instance
  metadata = { 
    group = "mac"
    ansible_user = "ubuntu"
  }
  image_id = "${data.openstack_images_image_v2.ubuntu-mac.id}" # Identifiant de l'image de la machine
  flavor_name = "b2-15" # Nom du type de machine
  key_pair = "${openstack_compute_keypair_v2.mac_keypair.name}"
  network = [
    {
      name = "Ext-Net"
    }
  ]
  security_groups = ["mac_prd"]
  user_data = "${data.template_cloudinit_config.mac.rendered}"

}

data "template_file" "mac" {
  template = "${file("${path.root}/mac.tpl")}"
}

data "template_cloudinit_config" "mac" {
  gzip          = false
  base64_encode = false

  part {
    content_type = "text/cloud-config"
    content      = "${data.template_file.mac.rendered}"
  }
}

output "mac.public_ip" {
  value = "${openstack_compute_instance_v2.mac.access_ip_v4}"
}
